import ora from 'ora';
import path from 'path';

const spinner = ora({
  text: '\x1b[36mCargando...\x1b[0m',
  spinner: {
    interval: 80,
    frames: ['⠋', '⠙', '⠹', '⠸', '⠼', '⠴', '⠦', '⠤', '⠔', '⠊'],
  },
}).start();

const totalBars = 30;
const total = 20;
// Función asíncrona para utilizar el ciclo for con async/await
async function simulateProgress() {
  for (let progress = 0; progress < total; progress++) {
    await new Promise(resolve => setTimeout(resolve, progress * 10));
    const fraction = `${progress}/${total}`;
    spinner.text = `\x1b[36mColaboradores ${fraction} | ${generateProgressBar(progress, totalBars, total)}\x1b[0m`;
    if (progress === total-1) {
      spinner.succeed('Completado');
      
      console.log("El nombre del archivo es:", path.basename(new Error().stack.split('\n')[1].split(' ').pop(), '.js').slice(0, -1));
            

      

    }
  }
  spinner.succeed(`Data de colaboradores - Completado ${total}`);
  spinner.fail('Data de colaboradores - Completado');
  spinner.info('Data de colaboradores - Completado');
  spinner.warn('Data de colaboradores - Completado');
}

function generateProgressBar(progress, totalBars, total) {
  const completedBars = Math.floor(progress / (total / totalBars));
  return `\x1b[36m[${'█'.repeat(completedBars)}${' '.repeat(totalBars - completedBars)}] ${Math.floor(
    (progress / total) * 100
  )}%\x1b[0m`;
}

// Llamada a la función asíncrona para iniciar la simulación del progreso
simulateProgress();
