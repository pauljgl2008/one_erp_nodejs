import puppeteer from "puppeteer";
import { createInterface } from "readline";
import fs from "fs";
import ExcelJS from "exceljs";
import nodemailer from "nodemailer";
import ora from "ora";
import path from "path";

const LOGIN_URL = "https://one.everis.com/apps";

(async () => {
  function generateProgressBar(progress, totalBars, total) {
    const completedBars = Math.floor(progress / (total / totalBars));
    return `\x1b[36m[${"█".repeat(completedBars)}${" ".repeat(
      totalBars - completedBars
    )}] ${Math.floor((progress / total) * 100)}%\x1b[0m`;
  }

  const updateProgressBar = async (i, total) => {
    const fraction = `${i}/${total}`;
    spinner.text = `\x1b[36mColaboradores ${fraction} | ${generateProgressBar(
      i,
      totalBars,
      total
    )}\x1b[0m`;
    if (i === total - 1) {
      spinner.start().succeed("Data de colaboradores - Completado");
    }
  };

  const ejecutarEmailReport = async (
    email,
    password,
    remitente,
    destinatario
  ) => {
    const file_name = await generateReport();
    await enviarCorreo(file_name, email, password, remitente, destinatario);
  };

  const loginWithRetry = async (maxLoginAttempts) => {
    let loginSuccess = false;
    for (let i = 0; i < maxLoginAttempts && !loginSuccess; i++) {
      try {
        let currentUrl = page.url();
        if (
          currentUrl.includes("https://adfsprod40.everis.com/adfs/ls/") &&
          i > 0
        ) {
          await page.waitForSelector("#differentVerificationOption", {
            visible: true,
          });
          await page.click("#differentVerificationOption");
          await page.waitForSelector("#verificationOption0", {
            visible: true,
          });
          await page.click("#verificationOption0");
        }
        loginSuccess = await autenticarConCodigoSMS(); // Assuming this function is defined elsewhere
        if (loginSuccess) {
          return loginSuccess; // Return as soon as loginSuccess becomes true
        }
      } catch (error) {
        spinner
          .start()
          .warn(
            `Intento de inicio de sesión fallido (${i + 1}/${maxLoginAttempts})`
          );
        console.log(
          `${error.toString()} ${path
            .basename(new Error().stack.split("\n")[1].split(" ").pop(), ".js")
            .slice(0, -1)}`
        );
      }
    }
    return loginSuccess; // Return the final value of loginSuccess
  };

  const obtenerCredenciales = async () => {
    const r0 = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const email = await new Promise((resolve) => {
      r0.question("Introduce tu email: ", (answer) => resolve(answer));
    });
    r0.close();
    const r1 = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const password = await new Promise((resolve) => {
      r1.question("Introduce tu password: ", (answer) => resolve(answer));
    });
    r1.close();
    const r2 = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const remitente = await new Promise((resolve) => {
      r2.question("Introduce tu nombre para el envío del correo: ", (answer) =>
        resolve(answer)
      );
    });
    r2.close();
    const r3 = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const destinatario = await new Promise((resolve) => {
      r3.question(
        "Introduce el correo del destinatario que se enviará el reporte: ",
        (answer) => resolve(answer)
      );
    });
    r3.close();
    return { email, password, remitente, destinatario };
  };
  const login = async (email, password) => {
    await page.goto(LOGIN_URL);
    await page.waitForNavigation({ waitUntil: "networkidle0" });
    await page.type("#userNameInput", email);
    await page.type("#passwordInput", password);
    await page.click("#submitButton");
  };

  const launchBrowser = async () => {
    return puppeteer.launch({
      headless: true,
      args: ["--start-maximized"],
      executablePath: "C:/Program Files/Google/Chrome/Application/chrome.exe", // Ruta de instalación de Chrome en Windows
    });
  };

  const obtenerCodigoSMS = async () => {
    const r3 = createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    const code = await new Promise((resolve) =>
      r3.question("Introduce el código de confirmación SMS: ", (answer) =>
        resolve(answer)
      )
    );
    r3.close();
    return code;
  };

  const autenticarConCodigoSMS = async () => {
    const smsCode = await obtenerCodigoSMS();
    await page.type("#oneTimePasscodeInput", smsCode);
    spinner.text = "\x1b[36mAutenticando con SMS...\x1b[0m";
    spinner.start();
    await Promise.all([
      page.click("#authenticateButton"),
      page.waitForNavigation({ waitUntil: "networkidle0" }),
    ]);
    let currentUrl = page.url();
    if (currentUrl.includes("https://adfsprod40.everis.com/adfs/ls/")) {
      spinner.fail("Failed to log in");
      return false;
    } else {
      spinner.info("Successfully logged in");
      return true;
    }
  };

  const obtenerFechaHora = async () => {
    const fecha_actual = new Date();
    const anio = fecha_actual.getFullYear();
    const mes = fecha_actual.getMonth() + 1;
    const dia = fecha_actual.getDate();
    const hora = fecha_actual.getHours();
    const minutos = fecha_actual.getMinutes();
    const segundos = fecha_actual.getSeconds();
    return `${dia}/${mes}/${anio} ${hora}:${minutos}:${segundos}`;
  };

  const generateReport = async () => {
    try {
      const projects_code = [
        { codigo: "ADM-001073-00001-01", descripcion: "Availability" },
        { codigo: "ADM-001073-00001-09", descripcion: "Concilia" },
        { codigo: "ADM-001073-00001-10", descripcion: "Entrance Course" },
        { codigo: "ADM-001073-00001-14", descripcion: "Legal Absence" },
        { codigo: "ADM-001073-00001-16", descripcion: "Mentoring" },
        { codigo: "ADM-001073-00001-22", descripcion: "Sickness" },
        {
          codigo: "ADM-001073-00001-25",
          descripcion: "Specific UN/US Training",
        },
        { codigo: "ADM-001073-00001-26", descripcion: "Technical Training" },
        { codigo: "ADM-001073-00001-27", descripcion: "Temporaly Disabled" },
        { codigo: "ADM-001073-00001-30", descripcion: "UCO Online Training" },
        { codigo: "ADM-001073-00001-31", descripcion: "Unpaid License" },
        { codigo: "ADM-001073-00001-32", descripcion: "Vacation" },
        { codigo: "CAR-001077-00020", descripcion: "Operativa Trujillo FY17" },
      ];
      let jsonData = JSON.parse(fs.readFileSync("reporte_one_erp.json"));
      const workbook = new ExcelJS.Workbook();
      function createSheet(workbook, sheetName, columns, data) {
        const sheet = workbook.addWorksheet(sheetName);
        sheet.columns = columns;
        data.forEach((item) => {
          sheet.addRow(item);
        });
      }
      const employeesColumns = [
        { header: "Nombre", key: "full_name" },
        { header: "Empleado ID", key: "employee_id" },
        { header: "Persona ID", key: "person_id" },
        { header: "Empresa", key: "company" },
        { header: "Último día para TL", key: "last_day_for_tl" },
        { header: "Horas pendientes", key: "pending_hours" },
        { header: "Días de vacaciones", key: "vacation_days" },
        { header: "Estado", key: "status" },
        { header: "Comentarios", key: "comments" },
      ];
      const timeDataSheetColumns = [
        { header: "Nombre completo", key: "full_name" },
        { header: "Periodo", key: "period" },
        { header: "Horas registradas", key: "recordedHours" },
        { header: "Fecha de envío", key: "submissionDate" },
        { header: "Estado", key: "status" },
      ];
      const projectsSheetColumns = [
        { header: "Nombre completo", key: "full_name" },
        { header: "Código de proyecto", key: "project_number" },
        { header: "Descripción del proyecto", key: "project_description" },
        { header: "Tipo de proyecto", key: "project_type" },
        { header: "Horas de proyecto", key: "project_hours" },
        { header: "Estado", key: "project_status" },
        { header: "Comentarios", key: "comments" },
      ];
      const timecardsDataSheetColumns = [
        { header: "Nombre completo", key: "full_name" },
        { header: "Periodo", key: "period" },
        { header: "Horas registradas", key: "recordedHours" },
        { header: "Fecha de envío", key: "submissionDate" },
        { header: "Estado", key: "status" },
        { header: "Código de proyecto", key: "project_number" },
        { header: "Descripción de proyecto", key: "project_description" },
        { header: "Tipo de proyecto", key: "project_type" },
        { header: "Horas de proyecto", key: "project_hours" },
        { header: "Estado", key: "project_status" },
        { header: "Comentarios", key: "comments" },
      ];
      // Crear hoja para los empleados
      createSheet(
        workbook,
        "Empleados",
        employeesColumns,
        jsonData.employees.flatMap((employee) => {
          return {
            full_name: employee.full_name,
            employee_id: employee.employee_id,
            person_id: employee.person_id,
            company: employee.company,
            last_day_for_tl: employee.last_day_for_tl,
            pending_hours: employee.pending_hours,
            vacation_days: employee.vacation_days,
            status: employee.pending_hours > 0 ? "alert" : "ok",
            comments:
              employee.pending_hours > 0
                ? `Faltan incurrir ${employee.pending_hours} horas`
                : "",
          };
        })
      );
      // Crear hoja para los datos de tiempo
      createSheet(
        workbook,
        "Periodos",
        timeDataSheetColumns,
        jsonData.employees.flatMap(({ full_name, data }) =>
          data.map(({ period, recordedHours, submissionDate, status }) => ({
            full_name,
            period,
            recordedHours,
            submissionDate,
            status,
          }))
        )
      );
      // Crear hoja para los proyectos
      createSheet(
        workbook,
        "Proyectos",
        projectsSheetColumns,
        jsonData.employees.flatMap((employee) =>
          employee.projects.flatMap((projectList) =>
            projectList.map((project) => {
              let projectExists = projects_code.some(
                (projectCode) => projectCode.codigo === project.project_number
              );
              return {
                full_name: employee.full_name,
                project_number: project.project_number,
                project_description: project.project_description,
                project_type: project.project_type,
                project_hours: project.project_hours,
                project_status: projectExists ? "ok" : "alert",
                comments: projectExists
                  ? ""
                  : "El código de proyecto no es correcto",
              };
            })
          )
        )
      );
      // Crear hoja para los datos de tarjetas de tiempo
      createSheet(
        workbook,
        "Timecards",
        timecardsDataSheetColumns,
        jsonData.employees.flatMap((employee) =>
          employee.timecards_data.flatMap((td) =>
            td.projects.map((p) => {
              let projectExists = projects_code.some(
                (projectCode) => projectCode.codigo === p.project_number
              );
              return {
                full_name: employee.full_name,
                period: td.period,
                recordedHours: td.recordedHours,
                submissionDate: td.submissionDate,
                status: td.status,
                project_number: p.project_number,
                project_description: p.project_description,
                project_type: p.project_type,
                project_hours: p.project_hours,
                project_status: projectExists ? "ok" : "alert",
                comments: projectExists
                  ? ""
                  : "El código de proyecto no es correcto",
              };
            })
          )
        )
      );
      function applyStylesToSheet(sheet) {
        // Aplicar estilo al encabezado
        const headerFill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "FFADD8E6" },
        };
        const bodyFill = {
          type: "pattern",
          pattern: "solid",
          fgColor: { argb: "FFFFFF" },
        };
        const cellFont = { bold: true, color: { argb: "000000" } };
        const cellAlignment = { vertical: "middle", horizontal: "left" };
        const cellBorder = {
          top: { style: "thin" },
          left: { style: "thin" },
          bottom: { style: "thin" },
          right: { style: "thin" },
        };
        const headerRow = sheet.getRow(1);
        headerRow.eachCell((cell) => {
          cell.fill = headerFill;
          cell.font = cellFont;
          cell.alignment = cellAlignment;
          cell.border = cellBorder;
          cell.width = 20;
        });
        sheet.eachRow((row, rowNumber) => {
          if (rowNumber !== 1) {
            let isAlert = false; // Variable para verificar si existe el valor "alert" en la columna "Estado"
            row.eachCell((cell, colNumber) => {
              cell.fill = bodyFill;
              cell.font = { ...cellFont, bold: false };
              cell.alignment = cellAlignment;
              cell.border = cellBorder;
              cell.width = 20;
              if (row.getCell(colNumber).value === "alert") {
                isAlert = true; // Actualiza la variable si encuentra el valor "alert"
              }
            });
            if (isAlert) {
              // Si existe el valor "alert" en la columna "Estado", aplica el estilo de relleno a toda la fila
              row.eachCell((cell) => {
                cell.fill = {
                  type: "pattern",
                  pattern: "solid",
                  fgColor: { argb: "FFFFFF00" }, // Cambia el color de relleno a amarillo
                };
              });
            }
          }
        });
        // Obtén el ancho máximo necesario para cada columna en el cuerpo y el encabezado
        const columnMaxWidths = [];
        const headerMaxWidths = [];
        sheet.eachRow((row, rowNumber) => {
          row.eachCell((cell, colNumber) => {
            const contentLength = cell.value ? cell.value.toString().length : 0;
            if (
              !columnMaxWidths[colNumber - 1] ||
              contentLength > columnMaxWidths[colNumber - 1]
            ) {
              columnMaxWidths[colNumber - 1] = contentLength;
            }
            if (
              rowNumber === 1 &&
              (!headerMaxWidths[colNumber - 1] ||
                contentLength > headerMaxWidths[colNumber - 1])
            ) {
              headerMaxWidths[colNumber - 1] = contentLength;
            }
          });
        });
        // Ajusta el ancho de las columnas en función del ancho máximo del cuerpo o del encabezado
        headerRow.eachCell((cell, colNumber) => {
          const maxWidth =
            Math.max(
              columnMaxWidths[colNumber - 1],
              headerMaxWidths[colNumber - 1]
            ) + 2; // Agrega un margen adicional de 2 caracteres
          sheet.getColumn(colNumber).width = maxWidth;
        });
      }
      // Llamar a la función para cada hoja de trabajo
      workbook.eachSheet((sheet) => {
        applyStylesToSheet(sheet);
      });
      // Guardar libro de Excel
      // Obtener la fecha y hora actual
      const fechaActual = new Date();
      // Extraer los componentes de la fecha y hora actual
      const año = fechaActual.getFullYear();
      const mes = fechaActual.getMonth() + 1;
      const dia = fechaActual.getDate();
      const hora = fechaActual.getHours();
      const minutos = fechaActual.getMinutes();
      const segundos = fechaActual.getSeconds();
      // Crear el código basado en el tiempo
      const codigo = `${año}${mes}${dia}${hora}${minutos}${segundos}`;
      let file_name = `reporte_one_erp_${codigo}.xlsx`;
      await workbook.xlsx
        .writeFile(file_name)
        .then(() => {
          spinner.start().succeed("Generar Reporte excel .xlsx - Completado");
        })
        .catch((error) => {
          spinner.start().fail(`Error al generar libro de Excel`);
          console.log(error.message);
        });
      return file_name;
    } catch (error) {
      // Captura la excepción y maneja el error
      spinner.start().fail(`Se produjo un error.`);
      console.log(error.message);
    }
  };

  const enviarCorreo = async (
    file_name,
    email,
    password,
    remitente,
    destinatario
  ) => {
    // Configurar el transporte de correo electrónico
    const transporter = nodemailer.createTransport({
      host: "smtp.office365.com",
      port: 587,
      secure: false,
      auth: {
        user: email, // Reemplazar con la dirección de correo electrónico de tu cuenta corporativa de Microsoft
        pass: password, // Reemplazar con la contraseña de tu cuenta corporativa de Microsoft
      },
    });
    const archivoAdjunto = await fs.promises.readFile(file_name);
    // Definir opciones de correo electrónico
    const mailOptions = {
      from: email, // Reemplazar con la dirección de correo electrónico de tu cuenta corporativa de Microsoft
      to: destinatario, // Reemplazar con el correo electrónico del destinatario
      subject: "Reporte de Registro de Horas Laborales en One ERP", // Asunto del correo electrónico
      text: `Estimado(a),\n\nAdjunto el reporte que contiene el registro de las horas laborales de los colaboradores del sistema One ERP. Por favor, revise la información y hágame saber si necesita más detalles.\n\nAtentamente,\n\n${remitente}`,
      attachments: [
        {
          filename: file_name, // Nombre del archivo adjunto
          content: archivoAdjunto, // Pasar el contenido del archivo leído
        },
      ],
    };
    // Enviar el correo electrónico
    try {
      const info = await transporter.sendMail(mailOptions);
      spinner
        .start()
        .succeed(`Correo electrónico enviado - Completado | ${info.response}`);
      return info.response;
    } catch (error) {
      console.log(
        `${error} ${path
          .basename(new Error().stack.split("\n")[1].split(" ").pop(), ".js")
          .slice(0, -1)}`
      );
      throw error;
    }
  };

  const spinner = ora({
    text: "\x1b[36mCargando...\x1b[0m",
    spinner: {
      interval: 80,
      frames: ["⠋", "⠙", "⠹", "⠸", "⠼", "⠴", "⠦", "⠤", "⠔", "⠊"],
    },
  });
  process.on("unhandledRejection", (reason, p) => {
    console.log("Unhandled Rejection at: Promise", p, "reason:", reason);
  });
  const { email, password, remitente, destinatario } =
    await obtenerCredenciales();
  const browser = await launchBrowser();
  const page = await browser.newPage();
  const screenSize = await page.evaluate(() => {
    return {
      width: 1536, //window.screen.width,
      height: 834, //window.screen.height,
    };
  });
  await page.setViewport(screenSize);
  const maxLoginAttempts = 3;
  let loginSuccess = false;
  await login(email, password);
  loginSuccess = await loginWithRetry(3);
  const totalBars = 30;
  if (loginSuccess) {
    spinner.text = "\x1b[36mCargando página One ERP...\x1b[0m";
    const start = new Date();
    // Aquí va el código que quieres medir
    await page.goto(
      "https://one.everis.com/apps?appState=lean#TimeLabor-display"
    );
    // Hacer clic en el botón "flecha" para desplegar la lista
    await page.waitForNavigation({ waitUntil: "networkidle0" });
    await page.waitForSelector("#__xmlview1--hboxChangeUser", {
      visible: true,
    });
    await page.waitForSelector("#__vbox8", { visible: true });
    let flechaButton = await page.$("#__vbox8");
    await flechaButton.click();
    // Esperar a que se cargue la lista y recorrer sus elementos
    await page.waitForSelector("#userDelegatedList-listUl > li", {
      visible: true,
    });
    const employeeList = await page.$$("#userDelegatedList-listUl > li");
    let employees = [];
    let fecha_actual = await obtenerFechaHora();
    let dataJSON = { employees, fecha_actual };

    const total = employeeList.length; // Número total de iteraciones
    spinner.start();
    for (let i = 0; i < employeeList.length; i++) {
      await page.waitForSelector("#userDelegatedList-listUl > li", {
        visible: true,
      });
      let employeeList2 = await page.$$("#userDelegatedList-listUl > li");
      await employeeList2[i].click();
      const text = await page.evaluate(
        (el) => el.textContent,
        employeeList2[i]
      );
      await page.waitForSelector("#__label5-bdi"); //esperar a que el div sea cargado
      await page.waitForFunction(
        (expectedText) => {
          let element = document.querySelector("#__label5-bdi");
          return element && element.textContent.includes(expectedText); //esperar a que el texto del div contenga la cadena de texto esperada
        },
        {},
        text
      );
      await page.waitForSelector("#__xmlview1--TimeCards-listUl", {
        visible: true,
      });
      // Obtener los datos requeridos
      const full_name = await page.$eval("#__label5-bdi", (element) =>
        element.textContent.trim()
      );
      const employee_id = await page.$eval("#__text8", (element) =>
        element.textContent.trim()
      );
      const [person_id, company] = await page.evaluate(() => {
        const elements = Array.from(
          document.querySelectorAll("#__vbox9 > div")
        );
        const values = elements.map((element) => element.textContent.trim());
        return values;
      });
      const last_day_for_tl = await page.$eval("#__label3-bdi", (element) =>
        element.textContent.trim()
      );
      const pending_hours = await page.$eval(
        "#__xmlview1--kpiPendingHours-bdi",
        (element) => element.textContent.trim()
      );
      const vacation_days = await page.$eval("#__label4-bdi", (element) =>
        element.textContent.trim()
      );
      // Selecciona los elementos de la tabla y devuelve un arreglo de objetos
      let employee = {
        full_name,
        employee_id,
        person_id,
        company,
        last_day_for_tl,
        pending_hours,
        vacation_days,
      };
      // Esperar a que aparezca el div de carga con el ID "spinner"
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        visible: true,
      });
      // Esperar a que el div de carga desaparezca del HTML
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        hidden: true,
      });
      const data = await page.evaluate(() => {
        const rows = document.querySelectorAll(
          "#__xmlview1--TimeCards-listUl > tbody > tr"
        );
        const dataArray = [];
        rows.forEach((row) => {
          const period = row.querySelector("td:nth-child(2)").innerText.trim();
          const recordedHours = row
            .querySelector("td:nth-child(3)")
            .innerText.trim();
          const submissionDate = row
            .querySelector("td:nth-child(4)")
            .innerText.trim();
          const status = row.querySelector("td:nth-child(5)").innerText.trim();
          const dataObj = {
            period,
            recordedHours,
            submissionDate,
            status,
          };
          dataArray.push(dataObj);
        });
        return dataArray;
      });
      let projects = [];
      let projectRows_master = await page.$$(
        "#__xmlview1--TimeCards-listUl > tbody > tr"
      );
      // Obtener todas las filas de la tabla
      for (let i = 0; i < projectRows_master.length; i++) {
        if (i > 0) {
          // Esperar a que aparezca el div de carga con el ID "spinner"
          await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
            visible: true,
          });
          // Esperar a que el div de carga desaparezca del HTML
          await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
            hidden: true,
          });
        }
        let projectRows = await page.$$(
          "#__xmlview1--TimeCards-listUl > tbody > tr"
        );
        let projectRow = projectRows[i];
        if (projectRow) {
          const rows = await page.$$("#__xmlview1--TimeCards-tblBody tr");
          // Obtener el último botón de la fila en la columna "actions"
          const actionButtons = await rows[i].$$(
            "#__xmlview1--TimeCards-tblBody .sapMListTblCell:nth-child(6) button"
          );
          const lastActionButton = actionButtons[actionButtons.length - 1];
          if (lastActionButton) {
            // Hacer clic en el último botón
            await lastActionButton.click();
          } else {
            spinner.start().fail("No se encontró ningún botón.");
          }
        } else {
          spinner.start().fail(`No se encontró la fila ${i}.`);
        }
        await page.waitForSelector("#__xmlview2-busyIndicator", {
          visible: true,
        });
        // Esperar a que el div de carga desaparezca del HTML
        await page.waitForSelector("#__xmlview2-busyIndicator", {
          hidden: true,
        });
        await page.waitForSelector("#__xmlview2--monthlySummary-listUl", {
          visible: true,
        });
        let lis = await page.$$("#__xmlview2--monthlySummary-listUl > li");
        let projects_detail = [];
        for (let iLi = 0; iLi < lis.length; iLi++) {
          let li = lis[iLi];
          let project_number = "";
          let project_description = "";
          let project_type = "";
          let project_hours = "";
          let label = await li.$(".sapMLabel");
          if (label) {
            project_number = await label.evaluate((el) => el.textContent);
          }
          let description = await li.$(".sapMText.sapMTextMaxWidth");
          if (description) {
            project_description = await description.evaluate(
              (el) => el.textContent
            );
          }
          let type = await li.$(`#__text55-__xmlview2--monthlySummary-${iLi}`);
          if (type) {
            project_type = await type.evaluate((el) => el.textContent);
          }
          let hours = await li.$(
            `#__title8-__xmlview2--monthlySummary-${iLi}-inner`
          );
          if (hours) {
            project_hours = await hours.evaluate((el) => el.textContent);
          }
          projects_detail.push({
            project_number,
            project_description,
            project_type,
            project_hours,
          });
        }
        projects.push(projects_detail);
        const backButton = await page.$("#backBtn");
        await backButton.click();
      }
      const timecards_data = [];
      for (let i = 0; i < data.length; i++) {
        const currentData = data[i];
        const currentProjects = projects[i];
        const currentDataWithProjects = {
          ...currentData,
          projects: currentProjects,
        };
        timecards_data.push(currentDataWithProjects);
      }
      employee["data"] = data;
      employee["projects"] = projects;
      employee["timecards_data"] = timecards_data;
      dataJSON.employees.push(employee);
      // Esperar a que aparezca el div de carga con el ID "spinner"
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        visible: true,
      });
      // Esperar a que el div de carga desaparezca del HTML
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        hidden: true,
      });
      let backBtn = await page.$("span#__xmlview1--iconLoggedUser");
      await backBtn.click();
      // Esperar a que aparezca el div de carga con el ID "spinner"
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        visible: true,
      });
      // Esperar a que el div de carga desaparezca del HTML
      await page.waitForSelector("#__xmlview1--TimeCards-busyIndicator", {
        hidden: true,
      });
      await page.waitForSelector("#__vbox8", { visible: true });
      let flechaBtn = await page.$("#__vbox8");
      await flechaBtn.click();
      await updateProgressBar(i, total);
    }
    // Obtener la fecha y hora actual después de la ejecución del código
    const end = new Date();
    // Restar la fecha y hora actual posterior a la anterior para obtener la duración de la ejecución en milisegundos
    const duration = end.getTime() - start.getTime();
    // Convertir los milisegundos a minutos, segundos y milisegundos, y formatear el resultado en una cadena en el formato "mm:ss:milisegundos"
    const minutes = Math.floor(duration / 60000);
    const seconds = Math.floor((duration % 60000) / 1000);
    const milliseconds = duration % 1000;
    const formattedTime = `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}:${milliseconds.toString().padStart(3, "0")}`;
    dataJSON["execution_time"] = formattedTime;
    fs.writeFileSync("reporte_one_erp.json", JSON.stringify(dataJSON, null, 2));
    spinner.start().succeed("Reporte reporte_one_erp.json - Completado");
    await ejecutarEmailReport(email, password, remitente, destinatario);
    await browser.close();
  } else {
    spinner
      .start()
      .warn(
        `Número máximo de intentos (${maxLoginAttempts}) superado. El inicio de sesión falló.`
      );
    await browser.close();
  }
})();
