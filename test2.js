import axios from 'axios';

const config = {
    headers: {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:98.0) Gecko/20100101 Firefox/98.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1",
        "Sec-Fetch-Dest": "document",
        "Sec-Fetch-Mode": "navigate",
        "Sec-Fetch-Site": "none",
        "Sec-Fetch-User": "?1",
        "Cache-Control": "max-age=0",
        Cookie: 'FedAuth=77u/PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48U1A+VjEyLDBoLmZ8bWVtYmVyc2hpcHx1cm4lM2FzcG8lM2Fhbm9uIzQ1ODlmMjVkZjM2NmFjNTkyYzg2YmYwMjUzZmRkYzRjZmIxZjQwYjliZDZiNzUyM2M4NTBkODZjOGYzZjgxODAsMCMuZnxtZW1iZXJzaGlwfHVybiUzYXNwbyUzYWFub24jNDU4OWYyNWRmMzY2YWM1OTJjODZiZjAyNTNmZGRjNGNmYjFmNDBiOWJkNmI3NTIzYzg1MGQ4NmM4ZjNmODE4MCwxMzMyNzEwODEwNTAwMDAwMDAsMCwxMzMyNzE5NDIwNTAxOTYyMjksMC4wLjAuMCwyNTgsMjBiYTA1NDYtNDg0OC00NDZhLWE4ZjAtYjQyZmMyNzI3OGNlLCwsZDM2ZmFkYTAtZTBlZC02MDAwLTkwZTQtNTJiN2U5Y2JkM2VkLGQzNmZhZGEwLWUwZWQtNjAwMC05MGU0LTUyYjdlOWNiZDNlZCxHSjdpWEd2WlRFYVBacUE3V3VEdXpnLDAsMCwwLCwsLDI2NTA0Njc3NDM5OTk5OTk5OTksMCwsLCwsLCwwLCxjeGoxaERtOHJYSFlvTzNpZEREMkxaSi9XTFpRd2VSaXhGMzhTSWtVWEhoTzM0eFpjQXNXVjZrRENGN2MzZ0U0VXVsT1ROcjlENEpsTGM1bk9VM1JmdG9PczNXd20vZFZsSUVxODE0WkFiVzR0b09ZM3pkeURSbHM0SWErdUJ2QVZDSUpkZXZZak5PeTBNcHM5MjZZR2hwSTJyMnhYL0NUMWk2UXArYWxJSnNVdHQwbkZDamppcGh3VXZkU3NIUlZkd21FeCtRWWQwNEFiNjRBaHQ5SDJiSVFlOE83ZmQ1SURNeGVENk9xMUJXSFdIKytxM001R0RhLzFDTmdJRGw2TndSVHh5RG9nTDlaMzdzUEpGMzdqSVJoWG1WbEIzQ0tlcEY5eWdTd0ZLTVdoSUUyMm5SV3BxR1Qxa1dSTmRxOFExRWRLcEo0N0czWFhsRWh0WjV1bUE9PTwvU1A+'
    }
};

axios.get('https://everisgroup-my.sharepoint.com/:x:/g/personal/pguevarl_emeal_nttdata_com/Eamb-WedFD1Nuqfa8yNfRncB4OKlC4Ytzg-N44cOb2DneA?e=iNfVdZ', config)
    .then((response) => {
        console.log(response.data);
    })
    .catch((error) => {
        console.error(error);
    });
